<?php
session_start();
ob_start();

/*if($_SERVER["HTTPS"] != "on")
{
    header("Location: https://".$_SERVER["HTTP_HOST"].$_SERVER["REQUEST_URI"]);
    exit();
}*/

header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
$root = 'http://localhost/php_test_task';

$dbhost = "";
$dbname = "php_test_task";
$dbuser = "root";
$dbpass = "";

try
{
    $pdo = new PDO("mysql:host=$dbhost;dbname=$dbname", $dbuser, $dbpass, array(PDO::ATTR_PERSISTENT => false));
}
catch (PDOException $e)
{

    print "Error!: Database Connection Error<br/>";
    die();
}

$script_location = ''; //for folders /test or /test1/test2

if (defined("IN_CRONTAB"))
{
    $_SERVER['REQUEST_SCHEME'] = 'https://';
    $_SERVER['HTTP_HOST'] = 'localhost/php_test_task';
    $_SERVER['REQUEST_URI'] = "";
}



$_SEO = array();

if(strpos($script_location, '/') !== false)
{
  $in_folder = explode($script_location, $_SERVER['REQUEST_URI']);
  $next = $in_folder[1];
  foreach (explode('/', $next) as $key => $value)
  {
    $_SEO[$key] = $value;
  }
}
else
{
    if(isset($_SERVER['REQUEST_URI'])) {
        foreach (explode('/', $_SERVER['REQUEST_URI']) as $key => $value) {
            $_SEO[$key] = $value;
        }
    }
}

unset($_SEO[0]);    

?>
