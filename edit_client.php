
<style>
    .send-button{
background: #54C7C3;
width:100%;
font-weight: 600;
color:#fff;
padding: 8px 25px;
}
input[type=number]::-webkit-inner-spin-button, 
input[type=number]::-webkit-outer-spin-button { 
  -webkit-appearance: none; 
  margin: 0; 
}
.g-button{
color: #fff !important;
border: 1px solid #EA4335;
background: #ea4335 !important;
width:100%;
font-weight: 600;
color:#fff;
padding: 8px 25px;
}
.my-input{
box-shadow: inset 0 1px 2px rgba(0, 0, 0, 0.1);
cursor: text;
padding: 8px 10px;
transition: border .1s linear;
}
.header-title{
margin: 5rem 0;
}
h1{
font-size: 31px;
line-height: 40px;
font-weight: 600;
color:#4c5357;
}
h2{
color: #5e8396;
font-size: 21px;
line-height: 32px;
font-weight: 400;
}
.login-or {
position: relative;
color: #aaa;
margin-top: 10px;
margin-bottom: 10px;
padding-top: 10px;
padding-bottom: 10px;
}
.span-or {
display: block;
position: absolute;
left: 50%;
top: -2px;
margin-left: -25px;
background-color: #fff;
width: 50px;
text-align: center;
}
.hr-or {
height: 1px;
margin-top: 0px !important;
margin-bottom: 0px !important;
}
@media screen and (max-width:480px){
h1{
font-size: 26px;
}
h2{
font-size: 20px;
}
}
</style>
    <?php 
        if(isset($_POST['name']))
        {
            $client_id = $_POST["id"];
            $q = $pdo->prepare('SELECT * FROM `cities` WHERE name =?');
            $q->execute(array($_POST['city']));
            $selected_city = $q->fetch(PDO::FETCH_ASSOC);
            if(empty($selected_city))
            {
                $sql = "INSERT INTO cities (name) VALUES (?)";
                $stmt= $pdo->prepare($sql);
                $stmt->execute([$_POST['city']]);
                $city = $pdo->lastInsertId();

            } else {
                $city = $selected_city['id'];
            }

            $name = $_POST['name'];
            $code = $_POST['code'];
            $profile = "";
            if(!empty($_FILES["profile"]["name"]))
            {
            $filename = $_FILES["profile"]["name"];
            $tempname = $_FILES["profile"]["tmp_name"];    
            $folder = "clients_profiles/".$filename;
                if (move_uploaded_file($tempname, $folder))  {
                    $profile = $folder;
                }   
            }
           $sql = "UPDATE  clients SET name= '$name', code= '$code', city= $city";
           if($profile!="")
           $sql .= ", picture='$profile'";

           $sql .= " WHERE id = $client_id";
           $stmt= $pdo->prepare($sql);
           $stmt->execute();
           header('Location: '.$root.'/clients');
        }
        $q = $pdo->prepare('SELECT * FROM `clients` WHERE id =?');
        $q->execute(array($_SEO[count($_SEO)-1]));
        $client = $q->fetch(PDO::FETCH_ASSOC);

        $q = $pdo->prepare('SELECT * FROM `cities` WHERE id =?');
        $q->execute(array($client['city']));
        $city = $q->fetch(PDO::FETCH_ASSOC);
    ?>
   <div class="" style="margin-top: 50px;">
      <div class="col-md-6 mx-auto text-center">
         <div class="header-title">
            <h1 class="wv-heading--title">
               Edit Client Form
            </h1>
         </div>
      </div>
      <br>
      <div class="row">
         <div class="col-md-4 mx-auto">
            <div class="myform form ">
               <form action="" method="post" enctype="multipart/form-data">
                   <input type="hidden" name="id" value="<?= $client['id']?>">
                  <div class="form-group">
                     <input type="text" required name="name" value="<?= $client['name']?>" class="form-control my-input" id="name" placeholder="Name">
                  </div>
                  <div class="form-group">
                     <input type="text" required name="code" value="<?= $client['code']?>" class="form-control my-input" id="code" placeholder="Code">
                  </div>
                  <div class="form-group">
                     <input type="text" required  name="city" id="city" value="<?php if(!empty($city)){echo $city['name'];}else{echo "";}  ?>"  class="form-control my-input" placeholder="City">
                  </div>
                  <div class="form-group">
                     <input type="file"  name="profile" accept="image/png, image/gif, image/jpeg" id="profile"  class="form-control my-input">
                  </div>
                  <div class="text-center ">
                     <button type="submit" class=" btn btn-block send-button tx-tfm">UpDate Client</button>
                  </div>
                  
                  <div class="col-md-12 ">
                     <div class="login-or">
                        <hr class="hr-or">
                     </div>
                  </div>
               </form>
            </div>
         </div>
      </div>
   </div>
   </div>
    </div>

    </div>

</body>
<script>
     $("#city").keyup(function(){
        var search_for = $(this).val();
        if(search_for!="")
        {
            $.ajax({
               method : 'POST',
               url : '<?= $root ?>/search_city',
               data  : {search_for : search_for},
               success:function(response)
               {
                response = JSON.parse(response);
                $( "#city" ).autocomplete({
                    source: response
                });

               } 
            });
        }
    });

</script>