<div class="user-dashboard">
    <h1>Hello, <?= $_SESSION['username'] ?></h1>  
</div>
<?php 
    $q = $pdo->prepare('SELECT * FROM `clients`');
    $q->execute();
    $clients = $q->fetchAll();
?>
<table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>Name</th>
                <th>City</th>
                <th>Code</th>
                <th>Profile</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
        <?php foreach($clients as $client){ 
             $q = $pdo->prepare('SELECT * FROM `cities` WHERE id =?');
             $q->execute(array($client['city']));
             $city = $q->fetch(PDO::FETCH_ASSOC);
            ?>
            <tr>
                <td><?= $client['name'] ?></td>
                <td><?php if(!empty($city)){echo $city['name']; }else{echo "";} ?></td>
                <td><?= $client['code'] ?></td>
                <td><img src="<?php if(empty($client['picture'])){echo ""; }else{ echo $root.'/'.$client['picture'];} ?>"></td>
                <td><a href="<?= $root.'/delete/'.$client['id']?>"><i class="fa fa-trash" aria-hidden="true"></i></a><a href="<?= $root.'/clients/'.$client['id'].'/edit'?>"><i class="fa fa-edit" style="margin-left:10px" aria-hidden="true"></i></a></td>
            </tr>
        <?php } ?>    
        </tbody>
    </table>
        </div>
    </div>

    </div>

</body>
<script>
    $(document).ready(function(){
        $('#example').DataTable({
            pageLength : 3,
        });
    });
</script>